# Sieve of Eratosthenes
This repository will contain a Go implementation of the sieve of Eratosthenes algorithm for finding prime numbers.

## Running the programme
1. cd path/to/directory
2. go build
3. ./directory -h
4. ./directory -file fileName.txt -limit someNumber