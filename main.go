package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

// Function for running the algorithm
func sieveOfEratosthenes(num int, filename string) {
	// Create array of "bools" to store true/false state of each number
	prime := make([]bool, num+1)

	// Set all numbers as default to true
	// meaning that if not changed, the number is a prime
	for i := 0; i < num+1; i++ {
		prime[i] = true
	}

	// Iterate through the numbers starting at the first prime "2"
	// and continue until the number^2 is <= than the number limit
	// if the number is still registered as true, then set all numbers
	// that are linked to that number to false
	for i := 2; i*i <= num; i++ {
		if prime[i] == true {
			for j := i * 2; j <= num; j += i {
				prime[j] = false
			}
		}
	}

	// Display primes in terminal or save to file
	savePrimes(num, prime, filename)
	//displayPrimes(num, prime)
}

// Function for displaying the prime numbers
func displayPrimes(num int, prime []bool) {
	fmt.Printf("Primes less than %d : ", num)

	// Print out all numbers registered as true, starting from "2"
	for i := 2; i < num; i++ {
		if prime[i] == true {
			fmt.Printf("%d ", i)
		}
	}

	fmt.Println()
}

// Function for saving primes in file
func savePrimes(num int, prime []bool, filename string) {
	clearFile(filename)

	for i := 2; i < num; i++ {
		if prime[i] == true {
			writePrimes(i, filename)
		}
	}
}

// Function for opening/writing to file
func writePrimes(prime int, filename string) {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	catchError(err)
	defer f.Close()

	_, err = f.WriteString(fmt.Sprintf("%d \n", prime))
	catchError(err)
}

// Function for clearing a file of contents
func clearFile(filename string) {
	_, err := os.OpenFile(filename, os.O_TRUNC|os.O_CREATE, 0644)
	catchError(err)
}

// Error function
func catchError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	// Flags
	limit := flag.Int("limit", 30, "Adjust the limit of prime search")
	fileName := flag.String("file", "primes.txt", "Choose the file for storing primes")
	flag.Parse()

	sieveOfEratosthenes(*limit, *fileName)
}
